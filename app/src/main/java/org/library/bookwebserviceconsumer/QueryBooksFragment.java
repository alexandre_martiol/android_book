package org.library.bookwebserviceconsumer;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.HashMap;
import java.util.Set;


public class QueryBooksFragment extends Fragment {

    private View rootView;
    /*private MainActivity mainActivityView;

    public QueryBooksFragment(MainActivity mainActivityView) {
        this.mainActivityView = mainActivityView;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        final EditText editext = (EditText) rootView.findViewById(R.id.editText);
        Button searchButton = (Button) rootView.findViewById(R.id.searchButton);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String author = editext.getText().toString();
                String title = "";
                Log.i("Texto del edit text", author);
                new BookRequestTask(QueryBooksFragment.this).execute(author, title);
            }
        });

        return rootView;
    }

    public void pintaLaLista (Book[] booksArray){
        final ListView listView = (ListView)getActivity().findViewById(R.id.listView);
        final SelectionAdapter booksArrayAdapter;
        booksArrayAdapter = new SelectionAdapter(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, booksArray);
        listView.setAdapter(booksArrayAdapter);

        //Context Action Bar definition
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            private int nr = 0;

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // TODO Auto-generated method stub
                booksArrayAdapter.clearSelection();
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // TODO Auto-generated method stub

                nr = 0;
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab, menu);
                mode.setTitle("My Library");
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {

                    case R.id.item_delete:
                        nr = 0;
                        booksArrayAdapter.clearSelection();
                        mode.finish();

                    case R.id.item_edit:
                        getFragmentManager().beginTransaction()
                                .replace(R.id.container, new EditBookFragment())
                                .commit();
                }
                return false;
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                // TODO Auto-generated method stub
                if (checked) {
                    nr++;
                    booksArrayAdapter.setNewSelection(position, checked);
                } else {
                    nr--;
                    booksArrayAdapter.removeSelection(position);
                }
                mode.setTitle(nr + " selected");

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                // TODO Auto-generated method stub

                listView.setItemChecked(position, !booksArrayAdapter.isPositionChecked(position));
                return false;
            }
        });


    }

    class SelectionAdapter extends ArrayAdapter<Book> {

        private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

        public SelectionAdapter(Context context, int resource,
                                int textViewResourceId, Book[] objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public void setNewSelection(int position, boolean value) {
            mSelection.put(position, value);
            notifyDataSetChanged();
        }

        public boolean isPositionChecked(int position) {
            Boolean result = mSelection.get(position);
            return result == null ? false : result;
        }

        public Set<Integer> getCurrentCheckedPosition() {
            return mSelection.keySet();
        }

        public void removeSelection(int position) {
            mSelection.remove(position);
            notifyDataSetChanged();
        }

        public void clearSelection() {
            mSelection = new HashMap<Integer, Boolean>();
            notifyDataSetChanged();
        }
    }
}