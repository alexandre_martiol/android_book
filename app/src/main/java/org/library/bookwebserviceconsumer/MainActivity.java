package org.library.bookwebserviceconsumer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity {

    //First commit by Warren

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new QueryBooksFragment())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new QueryBooksFragment())
                    .commit();
        }

        else if (id == R.id.action_create) {
            //Must to call to controller of fragment_create
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new CreateBookFragment())
                    .commit();
        }

        return super.onOptionsItemSelected(item);
    }
}