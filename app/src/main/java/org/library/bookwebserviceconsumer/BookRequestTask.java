package org.library.bookwebserviceconsumer;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;


public class BookRequestTask extends AsyncTask<String, Void, Book[]> {

    private QueryBooksFragment fragment;

    public BookRequestTask(QueryBooksFragment fragment) {

        this.fragment=fragment;
    }

    @Override
    protected Book[] doInBackground(String... params) {
        try {
            final HashMap<String, String> urlVariables = new HashMap<String, String>(
                    2);

            urlVariables.put("author", params[0]);
            urlVariables.put("title", params[1]);

            //MODIFICAR IP CON LA IP LOCAL DEL PC.
            final String url = "http://172.16.128.86:8080/books?author={author}&title={title}";


            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Book[] booksArray = restTemplate.getForObject(url, Book[].class, urlVariables);

            return booksArray;
        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Book[] booksArray) {
            /*TextView greetingIdText = (TextView) findViewById(R.id.id_value);
            TextView greetingContentText = (TextView) findViewById(R.id.content_value);*/
        Log.i("onPostExecute", Arrays.asList(booksArray).toString());
        //greetingIdText.setText(Arrays.asList(booksArray).toString());

        fragment.pintaLaLista(booksArray);
    }

}
