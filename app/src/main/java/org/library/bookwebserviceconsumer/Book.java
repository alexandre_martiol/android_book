package org.library.bookwebserviceconsumer;

public class Book {

	private long id;
	private String isbn;
	private String author;
	private String title;
	private int numSells;
	private String publishedDate;

    public Book() {
        super();
    }

    public Book(String isbn, String title,  String author, int numSells, String publishedDate) {
        this.isbn = isbn;
        this.author = author;
        this.title = title;
        this.numSells = numSells;
        this.publishedDate = publishedDate;
    }

    public String getAuthor() {
		return author;
	}

	public int getNumSells() {
		return numSells;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public long getId() {
		return id;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return "Title: " +  title + "\nAuthor: " + author;
	}
}