package org.library.bookwebserviceconsumer;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * Created by Alex on 15/12/14.
 */
public class BookCreateTask extends AsyncTask<String, Void, Void> {
    private CreateBookFragment fragment;
    private Book book;

    public BookCreateTask(CreateBookFragment createBookFragment, Book book) {
        this.fragment = fragment;
        this.book = book;
    }

    protected Void doInBackground(String... params) {
        try {

            //MODIFICAR IP CON LA IP LOCAL DEL PC.
            final String url = "http://localhost:8080/books";

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.postForObject(url, this.book, Book.class);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }
        return null;
    }
}
