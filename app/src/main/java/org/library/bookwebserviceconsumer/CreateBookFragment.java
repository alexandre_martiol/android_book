package org.library.bookwebserviceconsumer;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Created by Alex on 15/12/14.
 */
public class CreateBookFragment  extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_create, container, false);

        final EditText isbnEditText = (EditText) rootView.findViewById(R.id.isbnEditText);
        final EditText titleEditText = (EditText) rootView.findViewById(R.id.titleEditText);
        final EditText authorEditText = (EditText) rootView.findViewById(R.id.authorEditText);
        final EditText numSellsEditText = (EditText) rootView.findViewById(R.id.numSellsEditText);
        Button createButton = (Button) rootView.findViewById(R.id.createButton);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String isbn = isbnEditText.getText().toString();
                String title = titleEditText.getText().toString();
                String author = authorEditText.getText().toString();
                int numSells = Integer.parseInt(String.valueOf(numSellsEditText.getText()));

                Book book = new Book(isbn, title, author, numSells, "12/12/2014");

                new BookCreateTask(CreateBookFragment.this, book).execute();

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new QueryBooksFragment())
                        .commit();
            }
        });
        return rootView;
    }
}
