package org.library.bookwebserviceconsumer;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by AleM on 18/12/14.
 */
public class EditBookFragment extends Fragment {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_edit, container, false);

        final EditText isbnEditText = (EditText) rootView.findViewById(R.id.isbnEditText);
        final EditText titleEditText = (EditText) rootView.findViewById(R.id.titleEditText);
        final EditText authorEditText = (EditText) rootView.findViewById(R.id.authorEditText);
        final EditText numSellsEditText = (EditText) rootView.findViewById(R.id.numSellsEditText);
        Button editButton = (Button) rootView.findViewById(R.id.createButton);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String isbn = isbnEditText.getText().toString();
                String title = titleEditText.getText().toString();
                String author = authorEditText.getText().toString();
                int numSells = Integer.parseInt(String.valueOf(numSellsEditText.getText()));

                Book book = new Book(isbn, title, author, numSells, "12/12/2014");

                new BookEditTask(EditBookFragment.this).execute(book);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new QueryBooksFragment())
                        .commit();
            }
        });
        return rootView;
    }
}
