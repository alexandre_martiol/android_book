package org.library.bookwebserviceconsumer;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by AleM on 18/12/14.
 */
public class BookEditTask extends AsyncTask<Book, Void, Void> {
    private EditBookFragment fragment;

    public BookEditTask(EditBookFragment editBookFragment) {
        this.fragment = fragment;
    }

    protected Void doInBackground(Book... params) {
        try {

            //MODIFICAR IP CON LA IP LOCAL DEL PC.
            final String url = "http://localhost:8080/books";

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.put(url, params[0]);

        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }
        return null;
    }
}
